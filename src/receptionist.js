// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.
// <-start-
class Receptionist {
  constructor () {
    this.persons = [];
  }

  register (person) {
    // eslint-disable-next-line no-prototype-builtins
    if (person === undefined || person === null || person === '{}' || person.hasOwnProperty('id') === false || person.hasOwnProperty('name') === false || person === '') {
      // eslint-disable-next-line no-throw-literal
      throw 'Invalid person';
    }
    for (let i = 0; i < this.persons.length; i++) {
      if (person.id === this.persons[i].id) {
        // eslint-disable-next-line no-throw-literal
        throw 'Person already exist';
      }
    }
    this.persons.push(person);
    return this.persons;
  }

  getPerson (id) {
    for (let i = 0; i < this.persons.length; i++) {
      if (this.persons[i].id === id) {
        return this.persons[i];
      }
    }
    // eslint-disable-next-line no-throw-literal
    throw 'Person not found';
  }
}
// --end-->

export default Receptionist;
