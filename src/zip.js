// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (arrayLeft, arrayRight) {
  if (arrayLeft.length === 0 || arrayRight.length === 0) {
    return [];
  }

  if (arrayLeft.length === arrayRight.length) {
    const zippedArray = [];
    for (let i = 0; i < arrayLeft.length; i++) {
      zippedArray.push([arrayLeft[i], arrayRight[i]]);
    }
    return zippedArray;
  }

  if (arrayLeft.length < arrayRight.length) {
    arrayRight.splice(arrayLeft.length);
    const zippedArray = [];
    for (let i = 0; i < arrayLeft.length; i++) {
      zippedArray.push([arrayLeft[i], arrayRight[i]]);
    }
    return zippedArray;
  } else if (arrayLeft.length > arrayRight.length) {
    arrayLeft.splice(arrayRight.length);
    const zippedArray = [];
    for (let i = 0; i < arrayRight.length; i++) {
      zippedArray.push([arrayLeft[i], arrayRight[i]]);
    }
    return zippedArray;
  }
}
// --end->

export default zip;
